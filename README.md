- Problem: Recognize character and digit on medical record
- Approach: Employ the LeNet-5 CNN algorithm for Optical Character Recognition
- learned: Self-taught learning the fundamentals of CNN including normalize, reshape, one hot encode, building up model, plot accuracy, plot confusion matrix, predict test dataset 
